<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <title>Haskell 1: intro</title>

        <link rel="stylesheet" href="dist/reset.css">
        <link rel="stylesheet" href="dist/reveal.css">
        <link rel="stylesheet" href="dist/theme/black.css" id="theme">

        <!-- Theme used for syntax highlighted code -->
        <link rel="stylesheet" href="plugin/highlight/monokai.css" id="highlight-theme">
    </head>
    <body>
        <div class="reveal">
            <div class="slides">

                <section>
                    <h2 style="text-transform: none">Haskell 1: intro</h2>
                    <em>Basics & setup</em>
                </section>

                <section>

                    <section>
                        <em>Why?</em>
                        <p>The masses are <em>curious</em> but <em>uncommitted</em></p>
                    </section>

                    <section>
                        <em>Non-goals</em>
                        <div>
                            <p class="fragment">Teach Haskell properly</p>
                            <small class="fragment">Therefore, we'll freely use language extentions etc. to make a point</small>
                        </div>

                        <aside class="notes">
                            <ul>
                                <li>You can know Haskell if you practice</li>
                                <li>You can benefit by applying important principles in other environments</li>
                            </ul>
                        </aside>
                    </section>

                    <section>
                        <em>Goals</em>
                        <div>
                            <ul>
                                <li class="fragment fade-in-then-semi-out">Encourage individual learners</li>
                                <li class="fragment fade-in-then-semi-out">Demonstrate interesting ideas</li>
                                <li class="fragment fade-in-then-semi-out">Bust some myths</li>
                            </ul>
                        </div>
                    </section>

                    <!--

                    <section>
                        <em>GHC-specific language extensions</em>
                        <p>We'll freely use language extensions wherever we deem them useful to demonstrate a principle</p>
                        <div>
                            <ul>
                                <li><code>-XNoImplicitPrelude</code></li>
                                <li><code>-XOverloadedStrings</code></li>
                                <li><code>-XBangPatterns</code></li>
                                <li><code>-XTypeApplications</code></li>
                                <li><code>-XDeriveGeneric</code></li>
                            </ul>
                        </div>
                    </section>

                    -->

                    <section>It's always Question Time</section>
     
                </section>

                <section>

                    <section>
                        <em>Setup</em>
                        <div>
                            <ul>
                                <li class="fragment fade-in-then-semi-out">Install <code>stack</code></li>
                                <li class="fragment fade-in-then-semi-out"><code>$ stack ghci</code></li>
                                <li class="fragment fade-in-then-semi-out">Use containers (i.e. docker, podman, etc.)</li>
                                <li class="fragment fade-in-then-semi-out"><code>$ docker run --rm -it haskell</code></li>
                            </ul>
                        </div>
                    </section>

                    <section>
                        <em>Compiler - GHC</em>
                        <ul>
                            <li class="fragment fade-in-then-semi-out">Glasgow Haskell Compiler</li>
                            <li class="fragment fade-in-then-semi-out">Other implementations not nearly as widely used</li>
                            <li class="fragment fade-in-then-semi-out"><code>$ ghc Main.hs</code></li>
                            <li class="fragment fade-in-then-semi-out"><code>$ ghci</code></li>
                        </ul>
                    </section>

                    <section>
                        <em>Build tool - <code>stack</code></em>
                        <div>
                            <ul>
                                <li class="fragment fade-in-then-semi-out">FP Complete's build tool</li>
                                <li class="fragment fade-in-then-semi-out"><code>$ stack install yesod</code></li>
                                <li class="fragment fade-in-then-semi-out"><code>$ stack ghc Main.hs</code></li>
                                <li class="fragment fade-in-then-semi-out"><code>$ stack ghci</code></li>
                            </ul>
                        </div>
                    </section>

                </section>

                <section>

                    <section>
                        <em>Characteristics</em>
                        <div>
                            <ul>
                                <li class="fragment fade-in-then-semi-out">Statically typed</li>
                                <li class="fragment fade-in-then-semi-out">Non-strict (lazy) evaluation by default</li>
                                <li class="fragment fade-in-then-semi-out">Purity enforced via the type system</li>
                                <li class="fragment fade-in-then-semi-out">AOT compilation (not interpreted)</li>
                                <li class="fragment fade-in-then-semi-out">Whitespace matters (or explicit <code>{};</code>)</li>
                            </ul>
                        </div>
                    </section>

                    <section>
                        <h2 style="text-transform: none">Rationale</h2>
                        <em>Why is Haskell the way it is?</em>
                    </section>

                    <section>
                        <em>Imperative</em>

                        <div class="r-stack">

                            <pre class="fragment plaintext"><code data-trim>
x <- 3

x <- x + 1

x <- x * 2

// result in x
                            </code></pre>

                            <img class="fragment" src="static/imperative-diagram.png" alt="imperative code diagram"/>

                        </div>

                        <p class="fragment fade-in-then-semi-out">Main idea: state changes over time</p>

                    </section>

                    <section>
                        <em>Immutability</em>

                        <div class="r-stack">
                            <pre class="fragment plaintext"><code data-trim>
x <- 3

y <- x + 1

z <- y * 2

// result in z
                            </code></pre>

                            <img class="fragment" src="static/immutability-diagram.png" alt="immutability code diagram"/>
                        </div>

                        <p class="fragment fade-in-then-semi-out">Main idea: non-destructive updates preserve history</p>

                    </section>

                    <section>
                        <em>First class functions</em>

                        <div class="r-stack">
                            <pre class="fragment plaintext"><code data-trim>
x <- 3

f <- (+) 1

g <- (*) 2

g(f(x))
                            </code></pre>

                            <img class="fragment" src="static/first-class-functions-diagram.png" alt="first-class-functions code diagram"/>

                        </div>

                        <p class="fragment fade-in-then-semi-out">Main idea: focus on the operation, not on the state</p>

                    </section>

                    <section>
                        <em>Evaluation strategy</em>

                        <div class="r-stack">

                            <pre class="fragment plaintext"><code data-trim>
a <- get_a()
b <- get_b()
c <- make_c(a, b)
d <- make_d(a, c)
e <- make_e(b, d)
                            </code></pre>

                            <div class="fragment fade-in-then-out">
                                <p>As a graph</p>

                                <img class="evaluation-diagram" src="static/dependencies-ok-diagram.png" alt="A dependent calculation"/>
                            </div>

                            <div class="fragment fade-in-then-out">
                                <p><code>a</code> has failed</p>

                                <img class="evaluation-diagram" src="static/dependencies-bad-a-diagram.png" alt="A dependent calculation with a bad dependency"/>
                            </div>

                            <div class="fragment fade-in-then-out">
                                <p>Strict evaluation</p>

                                <img class="evaluation-diagram" src="static/dependencies-bad-all-diagram.png" alt="A dependent calculation all bad"/>
                            </div>

                            <div class="fragment fade-in-then-out">
                                <p>Non-strict evaluation</p>

                                <img class="evaluation-diagram" src="static/dependencies-undecided-diagram.png" alt="A dependent calculation undecided"/>
                            </div>

                            <div class="fragment fade-in-then-out">
                                <p>Possible outcome with non-strict evaluation</p>

                                <img class="evaluation-diagram" src="static/dependencies-bad-a-diagram.png" alt="A dependent calculation all ok"/>
                            </div>
                        </div>
                    </section>

                </section>

                <section>

                    <section>
                        <h2 style="text-transform: none">Haskell syntax</h2>
                        <em>The basics</em>
                    </section>

                    <section>
                        <em>Comments</em>

                        <pre class="fragment plaintext"><code data-trim>
-- single line comment
                        </code></pre>

                        <pre class="fragment plaintext"><code data-trim>
{- possibly
   multiline
   comment -}
                        </code></pre>

                    </section>

                    <section>
                        <em><code>$ ghci</code></em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude> :h[elp]
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude> :t[ype]
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude> :k[ind]
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude> :i[nfo]
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude> :se[t]
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude> :m[odule]
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude> :! vim Main.hs
                        </code></pre>

                    </section>

                    <section>
                        <em>Imports - source</em>

                        <div>
                            <pre style="display: inline-block; width: 49%;" class="haskell"><code data-trim>
-- Main.hs

-- Prelude

import Data.List

import Data.List (init)


import Data.List hiding (init)

import qualified Data.List as DL
                            </code></pre>

                            <pre style="display: inline-block; width: 49%;" class="python"><code data-trim>
# main.py

# builtins

from data.list import *

import data.list
from data.list import init

# no python equivalent

import data.list as dl
                            </code></pre>
                        </div>
                    </section>

                    <section>
                        <em>Imports - <code>ghci</code></em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> -- all "normal" forms, plus
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> :m + Data.List
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude Data.List λ> :m - Prelude
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Data.List λ> :m
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ>
                        </code></pre>

                    </section>

                    <section>
                        <em>Literals</em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> True
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> 'c'
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> 4
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> 8.3
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> "a string"
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> ['a', ' ', 's', 't', 'r', 'i', 'n', 'g']
                        </code></pre>

                    </section>

                    <section>
                        <em>More literals</em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> (1, False, 't')
                        </code></pre>


                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> [5,7..20]
                        </code></pre>


                        <pre class="fragment haskell"><code data-trim>
Prelude λ> \x -> not x
                        </code></pre>

                        <pre class="fragment python"><code data-trim>
>>> lambda x: not x
                        </code></pre>

                        <p class="fragment">Just like Python's, except shorter!</p>

                        <p class="fragment">... and <code>not</code> is a normal function</p>

                        <p class="fragment">... and everything's an exepression (no statements)</p>

                        <p class="fragment">... so lambdas are equivalent to named functions!</p>

                    </section>

                    <section>
                        <em>Variables</em>
                        <br/>
                        <small class="fragment"><p>Intentionally left blank</p></small>
                    </section>

                    <section>
                        <em>Named values / constants</em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> x = 4
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> double x = x * 2
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> double = \x -> x * 2
                        </code></pre>

                    </section>

                    <section>
                        <em>Conditionals</em>

                        <pre class="fragment haskell"><code data-trim>
Prelude λ> x = if 3 > 1 then 2 else 5
                        </code></pre>

                        <p class="fragment fade-in-then-semi-out">But I like ternaries?</p>

                        <pre class="fragment haskell"><code data-trim>
Prelude λ> x = (3 > 1) ?? 2 $ 5
                        </code></pre>

                        <p class="fragment"><code>(??)</code>, <code>($)</code> regular functions!</p>
                        <p class="fragment">We'll define then later</p>

                    </section>

                    <section>
                        <em>Guards</em>

                        <div class="r-stack">

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
myFunction x
    | x > 2     = print "Huge!"
    | True      = print "Nope."
                            </code></pre>


                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
myFunction x
    | x > 2     = print "Huge!"
    | otherwise = print "Nope."
                            </code></pre>

                        </div>

                    </section>

                    <section>
                        <em>Pattern matching</em>

                        <div class="r-stack">
                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
case x of
    0             -> print "Zero!"
    3             -> print "Ice cream"
    y | y > 0     -> print "not 0 and not 3"
      | otherwise -> print "negative"
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
myFunction 0  = print "Zero!"
myFunction 3  = print "Ice cream"
myFunction 82 = print "too big"
myFunction x
    | x > 0     = print "not 0, 3 or 82"
    | otherwise = print "negative"
                            </code></pre>

                        </div>

                    </section>

                </section>

                <section>Reminder: it's always Question Time</section>

                <section>

                    <section>
                        <h2 style="text-transform: none">Haskell 1 - type system</h2>
                    </section>

                    <section>
                        <em>Types as sets</em>

                        <div>
                            <ul>
                                <li class="fragment fade-in-then-semi-out">Think of "types" as "sets"</li>
                                <li class="fragment fade-in-then-semi-out">A type describes all possible values of a symbol</li>
                                <li class="fragment"><code>(::)</code> read as "&lt;left> is of type &lt;right>"</li>

                                <pre class="fragment haskell"><code data-trim>
    x :: Bool
                                </code></pre>

                                <li class="fragment fade-in-then-semi-out">Means <code>x</code> must be either <code>False</code> or <code>True</code></li>
                                <li class="fragment fade-in-then-semi-out">Not anything else*</li>
                                <li class="fragment fade-in-then-semi-out">Well actually, anything can be <code>⊥</code> (bottom)</li>
                            </ul>
                        </div>
                    </section>

                    <section>
                        <em>Literals revisited</em>

                        <div class="r-stack">
                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t True
True :: Bool
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t 'c'
'c' :: Char
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t "a string"
"a string" :: [Char]
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t 4
4 :: Num p => p
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t 8.3
8.3 :: Fractional p => p
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t (1, False, 't')
(1, False, 't') :: Num a => (a, Bool, Char)
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t [5,7..20]
[5,7..20] :: (Num a, Enum a) => [a]
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t \x -> not x
\x -> not x :: Bool -> Bool
                            </code></pre>

                        </div>

                    </section>

                    <section>
                        <em><code>Num</code>? <code>Fractional</code>? <code>Enum</code>?</em>

                        <ul>
                            <li class="fragment">
                                <p>Consider types as sets of terms</p>
                                <div class="fragment">
                                    <pre class="haskell"><code data-trim>
Bool := {False, True}
                                    </code></pre>
                                </div>
                            </li>
                            <li class="fragment">
                                <p>But types themselves are some-kind-of values...</p>
                                <pre class="fragment haskell"><code data-trim>
??? := {Int, Float, Bool, String}
                                </code></pre>
                            </li>

                            <br/>

                            <li class="fragment fade-in-then-semi-out">Such group of types isn't very useful</li>
                            <li class="fragment">
                                <p>We can think of groups that are</p>
                                <pre class="fragment haskell"><code data-trim>
Numerics := {Int, Float}
                                </code></pre>
                            </li>

                        </ul>
                    </section>

                    <section>The main topic of the next lecture ☺</section>

                    <section>
                        <em>Type annotations (expressions)</em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> five = 5
Prelude λ> :t five
five :: Num p => p
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> five = (5 :: Int)
Prelude λ> :t five
five :: Int
                        </code></pre>

                    </section>

                    <section>
                        <em>Type annotations (equations)</em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim data-line-numbers>
Prelude λ> add a b = a + b
Prelude λ> :t add
add :: Num a => a -> a -> a
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim data-line-numbers="2-3,5-6">
Prelude λ> :{
Prelude λ| add :: Int -> Int -> Int
Prelude λ| add a b = a + b
Prelude λ| :}
Prelude λ> :t add
add :: Int -> Int -> Int
                        </code></pre>

                    </section>

                    <section>
                        <em>Why are there multiple arrows?</em>

                        <pre class="fragment haskell"><code data-trim>
add1 ::  Int ->  Int -> Int
add1     a       b   =  a + b
                        </code></pre>

                        <pre class="fragment haskell"><code data-trim>
add2 ::  Int -> (Int -> Int)
add2     a   =  \b   -> a + b
                        </code></pre>

                        <pre class="fragment haskell"><code data-trim>
add3 :: (Int -> (Int -> Int))
add3 =  \a   -> \b   -> a + b
                        </code></pre>

                        <pre class="fragment haskell"><code data-trim>
add4 :: (Int ->  Int -> Int )
add4 =  \a       b   -> a + b
                        </code></pre>

                        <p class="fragment">All desugar to <code>add3</code></p>

                    </section>

                    <section>
                        <em>Prefix / infix</em>

                        <pre class="fragment haskell"><code data-trim>
add :: Int -> Int -> Int
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
add a b = a + b
                        </code></pre>

                        <pre class="fragment haskell"><code data-trim>
add a b = (+) a b
                        </code></pre>

                        <br/>

                        <pre class="fragment haskell"><code data-trim>
1 + 3 == 2 `add` 2
                        </code></pre>

                    </section>

                    <section>
                        <em>Currying (aka partial application)</em>

                        <pre class="fragment haskell"><code data-trim>
add :: Int -> Int -> Int
                        </code></pre>

                        <div class="r-stack">

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
add a b = a + b
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
add a b = (+) a b
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
add a   = (+) a
                            </code></pre>

                            <pre class="fragment haskell"><code data-trim>
add     = (+)
                            </code></pre>

                        </div>

                        <br/>

                        <div class="r-stack">

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t add
add     :: Int -> Int -> Int
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t add 1
add 1   ::        Int -> Int
                            </code></pre>

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
Prelude λ> :t add 1 2
add 1 2 ::               Int
                            </code></pre>

                        </div>

                    </section>

                    <section>
                        <em>Type synonyms - compiler-verified documentation</em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
_ :: Int -> a -> [a]
                        </code></pre>

                        <pre class="fragment haskell"><code data-trim>
_ :: String -> String -> IO ()
                        </code></pre>

                        <pre class="fragment haskell"><code data-trim>
Prelude λ> type FilePath = String
Prelude λ> type Content = String
                        </code></pre>

                        <pre class="fragment haskell"><code data-trim>
_ :: FilePath -> Content -> IO ()
                        </code></pre>

                    </section>

                    <section>
                        <em>More type synonyms</em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> type TwoInts = (Int, Int)
                        </code></pre>

                        <!-- TODO: Find out new non-deprecated syntax for constraints in type aliases

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> type TwoNumbers = (Num a, Num b) => (a, b)
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> type ANumberTwice = Num a => (a, a)
                        </code></pre>

                        -->

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> type Predicate a = a -> Bool
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> type List a = [a]
                        </code></pre>

                    </section>

                    <section>
                        <em>Remember <code>if</code>?</em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> x = if 3 > 1 then 2 else 5
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> x = (3 > 1) ?? 2 $ 5
                        </code></pre>

                        <p class="fragment fade-in-then-semi-out"><code>(??)</code>, <code>($)</code> regular functions!</p>

                        <p class="fragment fade-in-then-semi-out">We'll define then <del>later</del> now</p>

                    </section>

                    <section>
                        <em><code>(??)</code>, <code>($)</code> regular functions</em>

                        <pre class="fragment haskell"><code data-trim>
Prelude λ> y = (3 > 1) ?? 2 $ 5
                        </code></pre>

                        <p>

                        <pre class="fragment haskell"><code data-trim>
Prelude λ> :t ($)
($) :: (a -> b) -> a -> b
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
f $ a = f a
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
succ 3 === succ $ 3 === ($) succ 3
                        </code></pre>

                    </section>

                    <section>
                        <em>A couple more helpers</em>

                        <pre class="fragment haskell"><code data-trim>
Prelude λ> :t const
const :: a -> b -> a
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
const a _ = a
                        </code></pre>

                        <pre class="fragment haskell"><code data-trim>
Prelude λ> :t flip
flip :: (a -> b -> c) -> b -> a -> c
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
flip f b a = f a b
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> :t flip const
flip const :: b -> c -> c
                        </code></pre>

                    </section>

                    <section>
                        <em><code>(??)</code></em>

                        <div class="r-stack">

                            <pre class="fragment fade-in-then-out haskell"><code data-trim>
(??) :: Bool -> a ->  a -> a
                            </code></pre>

                            <pre class="fragment haskell"><code data-trim>
(??) :: Bool -> a -> (a -> a)
                            </code></pre>

                        </div>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> :{
Prelude λ| True ?? truthy = const truthy
Prelude λ| False ?? truthy = flip const truthy
Prelude λ| :}
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> (3 > 1) ?? 2 $ 5
2
                        </code></pre>

                    </section>

                    <section>
                        <em><code>map</code></em>

                        <pre class="fragment haskell"><code data-trim>
Prelude λ> :t map
map :: (a -> b) -> [a] -> [b]
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> map (\x -> x * 2) [1..3]
[2,4,6]
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> map ((*) 2) [1..3]
[2,4,6]
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> map (*2) [1..3]
[2,4,6]
                        </code></pre>

                    </section>

                    <section>
                        <em>Composition</em>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> map (\x -> 2 * (x + 1)) [1..3]
[4,6,8]
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> :t (.)
(.) :: (b -> c) -> (a -> b) -> a -> c
g . f = g (f x)
g . f = g $ f x
                        </code></pre>

                        <pre class="fragment fade-in-then-semi-out haskell"><code data-trim>
Prelude λ> map ((*2) . succ) [1..3]
[4,6,8]
                        </code></pre>

                    </section>

                </section>

                <section>Questions?</section>

            </div>
        </div>

        <script src="dist/reveal.js"></script>
        <script src="plugin/notes/notes.js"></script>
        <script src="plugin/markdown/markdown.js"></script>
        <script src="plugin/highlight/highlight.js"></script>
        <script src="plugin/search/search.js"></script>
        <script>
            // More info about initialization & config:
            // - https://revealjs.com/initialization/
            // - https://revealjs.com/config/
            Reveal.initialize({
                hash: true,

                // Learn about plugins: https://revealjs.com/plugins/
                plugins: [ RevealMarkdown, RevealHighlight, RevealNotes, RevealSearch ],

                totalTime: 60 * 60, // 1 hour in seconds

                slideNumber: "c/t",
            });
        </script>
        <style>
            .evaluation-diagram {
                /* Reveal.js automatically scales the resolution, so these hardcoded px measures scale fine (they scale the original size to 75% */
                width: 214px;
                height: 435px;
            }
        </style>
    </body>
</html>
